# coding=utf-8
from django.contrib import admin
from django.db import models
from django.contrib.humanize.templatetags.humanize import naturaltime

from mark2cure.document.models import Document, Pubtator, Section, View, Annotation
from mark2cure.common.templatetags.truncatesmart import truncatesmart


class DocumentAdmin(admin.ModelAdmin):
    search_fields = ('document_id', 'title', 'source')

    list_display = ('document_id', 'title_preview', 'sections',
                    'pubtator', 'annotations', 'completed_views',
                    'pending_views', 'source')

    readonly_fields = ('document_id', 'title', 'authors',
                       'source')

    mymodel = models.ForeignKey(Document)


def pending_views(obj):
    return View.objects.filter(section__document=obj, completed=False).count()


def completed_views(obj):
    return View.objects.filter(section__document=obj, completed=True).count()


def annotations(obj):
    return Annotation.objects.filter(view__section__document=obj).count()


def title_preview(obj):
    return truncatesmart(obj.title)


def pubtator(obj):
    return obj.valid_pubtator()


def sections(obj):
    return obj.count_available_sections()


class PubtatorAdmin(admin.ModelAdmin):
    search_fields = ('document__document_id', 'session_id', 'content', 'kind')

    list_display = ('pmid', 'kind', 'session_id',
                    'annotations', 'valid', 'request_count',
                    'updated', 'created',)

    readonly_fields = ('document', 'kind', 'session_id',
                       'content', 'validate_cache',
                       'request_count', 'updated', 'created',)

    @staticmethod
    def pmid(obj):
        """

        :rtype : object
        """
        return obj.document.document_id

    @staticmethod
    def valid(obj):
        return False != obj.valid

    @staticmethod
    def annotations(obj):
        return obj.count_annotations()

    mymodel = models.ForeignKey(Pubtator)


class SectionAdmin(admin.ModelAdmin):
    search_fields = ('text', 'document__document_id')

    list_display = ('text_preview', 'kind', 'updated',
                    'annotations', 'completed_views', 'pending_views',
                    'created', 'document')

    readonly_fields = ('kind', 'text', 'source',
                       'updated', 'created', 'document')

    @staticmethod
    def text_preview(obj):
        return truncatesmart(obj.text)

    @staticmethod
    def annotations(obj):
        return Annotation.objects.filter(view__section=obj).count()

    @staticmethod
    def completed_views(obj):
        return View.objects.filter(section=obj, completed=True).count()

    @staticmethod
    def pending_views(obj):
        return View.objects.filter(section=obj, completed=False).count()

    mymodel = models.ForeignKey(Section)


class ViewAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'opponent__user__username', 'section__document__document_id')

    list_display = ('task_type', 'section_preview', 'annotations',
                    'user', 'partner', 'quest', 'group', 'completed')

    readonly_fields = ('task_type', 'completed', 'opponent',
                       'section', 'user',)

    @staticmethod
    def section_preview(obj):
        return truncatesmart(obj.section.text)

    @staticmethod
    def partner(obj):
        if obj.opponent:
            return obj.opponent.user
        else:
            return '[new anns]'

    @staticmethod
    def annotations(obj):
        return Annotation.objects.filter(view=obj).count()

    @staticmethod
    def quest(obj):
        uqr = obj.userquestrelationship_set.first()
        if uqr:
            return uqr.task

    @staticmethod
    def group(obj):
        uqr = obj.userquestrelationship_set.first()
        if uqr:
            if uqr.task:
                from mark2cure.common.admin import group

                if group:
                    return group.stub

    mymodel = models.ForeignKey(View)


class AnnotationAdmin(admin.ModelAdmin):
    search_fields = ('view__user__username', 'view__section__document__document_id', 'text', 'kind', 'type')

    list_display = ('text', 'type', 'start',
                    'section', 'username', 'pmid',
                    'quest', 'group', 'time_ago',
                    'created')

    readonly_fields = ('kind', 'type', 'text',
                       'start', 'created', 'view')

    @staticmethod
    def section(obj):
        if obj.view:
            return obj.view.section.get_kind_display()

    @staticmethod
    def username(obj):
        if obj.view.user:
            return obj.view.user.username

    @staticmethod
    def pmid(obj):
        if obj.view.section:
            return obj.view.section.document.document_id

    @staticmethod
    def quest(obj):
        uqr = obj.view.userquestrelationship_set.first()
        if uqr:
            if uqr.task:
                return uqr.task

    @staticmethod
    def group(obj):
        uqr = obj.view.userquestrelationship_set.first()
        if uqr:
            if uqr.task:
                from mark2cure.common.admin import group

                if group:
                    return group.stub

    @staticmethod
    def time_ago(obj):
        return naturaltime(obj.created)

    mymodel = models.ForeignKey(Annotation)


admin.site.register(Document, DocumentAdmin)
admin.site.register(Pubtator, PubtatorAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(View, ViewAdmin)
admin.site.register(Annotation, AnnotationAdmin)
