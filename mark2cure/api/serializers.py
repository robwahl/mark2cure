# coding=utf-8
from mark2cure.common.models import Group, Task
from mark2cure.userprofile.models import UserProfile, Team

from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Group
        fields = ('pk', 'name', 'stub', 'description')


class UserProfileSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    score = serializers.SerializerMethodField()

    @staticmethod
    def get_name(profile):
        """

        :param profile: 
        :return: 
        """
        return profile.user.username

    @staticmethod
    def get_user(profile):
        """

        :param profile: 
        :return: 
        """
        user = profile.user
        return {'pk': user.pk,
                'username': user.username}

    @staticmethod
    def get_score(profile):
        """

        :param profile: 
        :return: 
        """
        return int(profile.score) if profile.score else 0

    class Meta(object):
        model = UserProfile
        fields = ('user', 'name', 'score', 'quote', 'motivation')


class TeamLeaderboardSerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField()

    class Meta(object):
        model = Team
        fields = ('name', 'score',)


def get_score(profile):
    """

    :param profile:
    :return:
    """
    return int(profile.score) if profile.score else 0


class QuestSerializer(serializers.ModelSerializer):
    """
    
    :param args: 
    :param kwargs: 
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        context = kwargs.pop('context', {})
        user = context.get('user', None)
        self.user_highest_level = user.profile.highest_level('skill').level

        # Instantiate the superclass normally
        super(QuestSerializer, self).__init__(*args, **kwargs)

    user = serializers.SerializerMethodField('get_user_status')
    progress = serializers.SerializerMethodField('get_progress_status')

    def get_user_status(self, task):
        """

        :param task: 
        :return: 
        """
        return {'enabled': self.user_highest_level >= task.requires_qualification,
                'completed': task.user_completed > 0}

    @staticmethod
    def get_progress_status(task):
        """


        :param task:
        :rtype : object
        """
        return {'required': task.completions if task.completions else 10000,
                'current': task.current_submissions_count,
                'completed': task.current_submissions_count >= task.completions if task.completions else 10000 <= task.current_submissions_count}

    class Meta(object):
        model = Task
        fields = ('id', 'name', 'documents', 'points',
                  'requires_qualification', 'provides_qualification',
                  'meta_url', 'user', 'progress')
