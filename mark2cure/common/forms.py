# coding=utf-8
from django import forms

from .models import SupportMessage


class SupportMessageForm(forms.ModelForm):
    class Meta(object):
        model = SupportMessage
