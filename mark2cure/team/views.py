# coding=utf-8

from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse

from mark2cure.userprofile.models import Team


def home(request, teamname):
    """

    :param request:
    :param teamname:
    :return:
    """
    team = get_object_or_404(Team, name=teamname)
    members = team.userprofile_set.select_related('user')
    ctx = {'team': team, 'members': members}
    return TemplateResponse(request, 'team/public-team.jade', ctx)
