# coding=utf-8
__all__ = ['BioCLocation']

import meta


class BioCLocation(meta._MetaOffsetProtect):
    def __init__(self, location=None):
        self.offset = '-1'
        self.length = '0'

        if location is not None:
            self.offset = location.offset
            self.length = location.length

    def __str__(self):
        s = str(self.offset) + ':' + str(self.length)

        return s
