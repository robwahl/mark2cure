# coding=utf-8
from django.utils import timezone

from .models import UserProfile


class ActiveUserMiddleware(object):
    def __init__(self):
        pass

    @staticmethod
    def process_request(request):
        current_user = request.user
        if current_user.is_authenticated():
            UserProfile.objects.filter(user=current_user).update(last_seen=timezone.now())
