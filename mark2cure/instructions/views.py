# coding=utf-8
from django.template.response import TemplateResponse


def disease_marking(request):
    """

    :param request:
    :return:
    """
    return TemplateResponse(request, 'instructions/disease-marking.jade')


def gene_marking(request):
    """

    :param request:
    :return:
    """
    return TemplateResponse(request, 'instructions/gene-marking.jade')


def treatment_marking(request):
    """

    :param request:
    :return:
    """
    return TemplateResponse(request, 'instructions/treatment-marking.jade')
