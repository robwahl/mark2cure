# coding=utf-8
from optparse import make_option
from collections import Counter

from django.core.management.base import BaseCommand

from mark2cure.document.models import Pubtator


class Command(BaseCommand):
    help = 'Utils for populating the system with Documents & Annotations'

    option_list = BaseCommand.option_list + (
        make_option('--keys',
                    action='store_true',
                    dest='keys',
                    default=False,
                    help='Inspect the bioc infon keys'),
    )

    @staticmethod
    def handle(**options):
        types_arr = []
        errors = 0

        if options['keys']:
            for _ in Pubtator.objects.filter(content__isnull=False).all():
                pass

            print 'Errors:', errors
            print Counter(types_arr)
