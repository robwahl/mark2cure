# coding=utf-8
__all__ = ['BioCDocument']

import compat
import meta


class BioCDocument(meta._MetaIdProtect, meta._MetaInfonsProtect, meta._MetaRelationsProtect, meta._MetaIterProtect,
                   compat._Py2NextProtect):
    def __init__(self, document=None):
        super(BioCDocument, self).__init__()
        self.id = ''
        self.infons = dict()
        self.relations = list()
        self.passages = list()
        if document is not None:
            self.id = document.id
            self.infons = document.infons
            self.relations = document.relations
            self.passages = document.passages

    def __str__(self):
        s = 'id: ' + self.id + '\n'
        s += 'infon: ' + str(self.infons) + '\n'
        s += str(self.passages) + '\n'
        s += 'relation: ' + str(self.relations) + '\n'

        return s

    def _iterdata(self):
        return self.passages

    def get_size(self):
        return self.passages.size()  # As in Java BioC

    def clear_passages(self):
        self.passages = list()

    def add_passage(self, passage):
        self.passages.append(passage)

    def remove_passage(self, passage):
        if type(passage) is int:
            self.passages.remove(self.passages[passage])
        else:
            self.passages.remove(passage)  # TBC
