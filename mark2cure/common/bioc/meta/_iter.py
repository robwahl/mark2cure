# coding=utf-8
__all__ = []


class _MetaIterProtect(object):
    def __init__(self):
        pass

    def __iter__(self):
        return self._iterdata().__iter__()
