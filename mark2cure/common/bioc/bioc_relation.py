# coding=utf-8
__all__ = ['BioCRelation']

from compat import _Py2NextProtect as _Py2Next
import meta


class BioCRelation(_MetaId, meta._MetaInfonsProtect, _Py2Next, meta._MetaIterProtect):
    def __init__(self, relation=None):
        super(BioCRelation, self).__init__()
        self.id = ''
        self.nodes = list()
        self.infons = dict()
        if relation is not None:
            self.id = relation.id
            self.nodes = relation.nodes
            self.infons = relation.infons

    def __str__(self):
        s = 'id: ' + self.id + '\n'
        s += 'infons: ' + str(self.infons) + '\n'
        s += 'nodes: ' + str(self.nodes) + '\n'

        return s

    def _iterdata(self):
        return self.nodes

    # Parameter 'node' unfilled
    def add_node(self, node, refid=None, role=None):
        # Discard arg ``node'' if optional args fully provided
        if (refid is not None) and (role is not None):
            self.add_node(refid=refid, role=role)
        else:  # Only consider optional args if both set
            self.nodes.append(node)
